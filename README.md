# IFS_Campusmap_get_GeoJson_from_OSM
Das Skript dient dazu, ein GeoJson aus einem bestimmten bereich (bbox) aus Open Street Map zu erstellen. 

## Setup

Um das Script nutzen zu können, muss Python 3.10 installiert sein.

### GDAL Installation

1. Aktualisiere die Paketliste:

   sudo apt update

2. Installiere GDAL und die erforderlichen Bibliotheken:

   sudo apt install gdal-bin libgdal-dev

3. Finde den Speicherort der GDAL-Konfiguration:

   which gdal-config

4. Setze die Umgebungsvariable `GDAL_CONFIG`:

   export GDAL_CONFIG=/usr/bin/gdal-config

5. Füge für die Persistenz den Pfad zur `.bashrc` hinzu:

   echo 'export GDAL_CONFIG=/usr/bin/gdal-config' >> ~/.bashrc
   source ~/.bashrc

### GDAL Version festlegen

6. Setze die GDAL-Version als Umgebungsvariable:

   export GDAL_VERSION=version

7. Füge die Version ebenfalls zur `.bashrc` hinzu:

   echo 'export GDAL_VERSION=version' >> ~/.bashrc
   source ~/.bashrc



## Verwendung:
1. pip install -r requirements.txt
2. [boundary.geojson](boundary.geojson) auf den Bereich anpassen, der abgefragt werden soll. (https://geojson.io)
![img.png](img/img.png)
3. Ausführen und [output.geojson](output.geojson) ist dein GeoJson.


